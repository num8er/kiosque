NewsifyRu::Application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  
  root to: 'dispatcher#detect'
  get '/about', to: 'dispatcher#about'
  get '/contacts', to: 'dispatcher#contacts'
  get '/:slug/(:id)', to: 'dispatcher#detect'


  #get '/categories', to: 'dispatcher#categories_json'
  #get '/news', to: 'dispatcher#news_json'
  #get '/feed', to: 'dispatcher#rss'
  #get '/:slug(/:id)', to: 'dispatcher#detect'
  #post '/like', to: 'dispatcher#like'
end
