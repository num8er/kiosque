namespace :parse_images do
  desc "Parsing images"
  task :parse_imgs => :environment do
    parse_imgs(ENV["id"])
  end
  
  def parse_imgs(id)
    p = Post.find(id)
    p.status = 1

    pa = PostAsset.where(post_id: id).first

    if pa
      p.main_image_url = pa.url
      p.main_image_by_url = p.main_image_url
      title = p.title.downcase

      p.is_photo = 1 if title.match(/(f|ph)oto/i)
      p.is_video = 1 if title.match(/v(i|I|ı|İ)deo/i)
    end
    
    p.save
  end
end