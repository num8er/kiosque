namespace :parse_slug_from_title do
  desc "Parsing slug"
  task :run => :environment do
    parse(ENV["post_id"])
  end
  
  def parse(post_id)
    post = Post.find(post_id)

    seo = Seo.new
    seo.title = post.title
    seo.seoable_id = post.id
    seo.seoable_type = "Post"
    seo.save
  end
end
