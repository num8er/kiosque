# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150211154121) do

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "categories", force: true do |t|
    t.string  "name",             null: false
    t.integer "sort", default: 1, null: false
  end

  add_index "categories", ["sort"], name: "sort", using: :btree

  create_table "channel_categories", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "channels", force: true do |t|
    t.integer  "category_id",                          null: false
    t.integer  "channel_category_id",                  null: false
    t.string   "title",                                null: false
    t.string   "channel_url",                          null: false
    t.string   "title_path"
    t.string   "descr_path"
    t.string   "image_path"
    t.string   "content_path"
    t.string   "news_item_path"
    t.string   "hyperlink_item_path"
    t.text     "not_allowed_path"
    t.string   "channel_type",        default: "html", null: false
    t.boolean  "status",              default: true,   null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "channels", ["channel_category_id"], name: "channel_category_id", using: :btree
  add_index "channels", ["channel_type"], name: "channel_type", using: :btree
  add_index "channels", ["status"], name: "status", using: :btree

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "post_assets", force: true do |t|
    t.integer  "post_id"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "post_assets", ["post_id"], name: "post_id", using: :btree

  create_table "posts", force: true do |t|
    t.integer  "category_id",                             null: false
    t.integer  "channel_id",                              null: false
    t.integer  "channel_category_id",                     null: false
    t.string   "title",                                   null: false
    t.text     "description"
    t.text     "body"
    t.string   "source"
    t.integer  "views",                   default: 0,     null: false
    t.integer  "rating",                  default: 0,     null: false
    t.boolean  "is_photo",                default: false, null: false
    t.boolean  "is_video",                default: false, null: false
    t.boolean  "is_main",                 default: false, null: false
    t.boolean  "view_type",               default: false, null: false
    t.boolean  "status",                  default: false, null: false
    t.string   "main_image_file_name"
    t.string   "main_image_content_type"
    t.integer  "main_image_file_size"
    t.datetime "main_image_updated_at"
    t.string   "main_image_url"
    t.integer  "self_share_count",        default: 0,     null: false
    t.integer  "self_like_count",         default: 0,     null: false
    t.integer  "self_comment_count",      default: 0,     null: false
    t.integer  "self_total_count",        default: 0,     null: false
    t.integer  "share_count",             default: 0,     null: false
    t.integer  "like_count",              default: 0,     null: false
    t.integer  "comment_count",           default: 0,     null: false
    t.integer  "total_count",             default: 0,     null: false
    t.integer  "total_total",             default: 0,     null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "posts", ["category_id"], name: "category_id", using: :btree
  add_index "posts", ["channel_category_id"], name: "channel_category_id", using: :btree
  add_index "posts", ["channel_id"], name: "channel_id", using: :btree
  add_index "posts", ["is_photo"], name: "is_photo", using: :btree
  add_index "posts", ["is_video"], name: "is_video", using: :btree
  add_index "posts", ["rating"], name: "rates", using: :btree
  add_index "posts", ["source"], name: "source", unique: true, using: :btree
  add_index "posts", ["status"], name: "status", using: :btree
  add_index "posts", ["view_type"], name: "view_type", using: :btree
  add_index "posts", ["views"], name: "views", using: :btree

  create_table "queries", force: true do |t|
    t.string   "query"
    t.string   "query_hash"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "query_stats", force: true do |t|
    t.integer  "query_id",   null: false
    t.integer  "ip",         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "query_stats", ["ip"], name: "ip", using: :btree
  add_index "query_stats", ["query_id"], name: "query_id", using: :btree

  create_table "rates", force: true do |t|
    t.integer  "post_id",    null: false
    t.integer  "ip",         null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "rates", ["ip"], name: "ip", using: :btree
  add_index "rates", ["post_id"], name: "post_id", using: :btree

  create_table "seos", force: true do |t|
    t.string  "title"
    t.string  "slug",                    null: false
    t.string  "slug_hash",    limit: 32, null: false
    t.integer "seoable_id",              null: false
    t.string  "seoable_type", limit: 16, null: false
  end

  add_index "seos", ["seoable_id", "seoable_type"], name: "seoable", using: :btree
  add_index "seos", ["slug"], name: "slug", unique: true, using: :btree
  add_index "seos", ["slug_hash"], name: "slug_hash", unique: true, using: :btree

  create_table "similars", force: true do |t|
    t.integer "f_id", null: false
    t.integer "s_id", null: false
  end

  add_index "similars", ["f_id", "s_id"], name: "fs_id", unique: true, using: :btree

end
