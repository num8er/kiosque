class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories, :id => false do |t|
      t.column :id, 'INT UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY (id)'
      t.string :name, null: false
      t.column :sort, 'integer unsigned', null: false, default: 1
    end

    add_index :categories, :sort, :name => 'sort'
  end
end
