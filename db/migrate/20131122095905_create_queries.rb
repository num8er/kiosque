class CreateQueries < ActiveRecord::Migration
  def change
    create_table :queries, :id => false do |t|
      t.column :id, 'INT UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY (id)'
      t.string :query
      t.string :query_hash

      t.timestamps
    end
  end
end
