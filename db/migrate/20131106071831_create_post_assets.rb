class CreatePostAssets < ActiveRecord::Migration
  def change
    create_table :post_assets, :id => false do |t|
      t.column :id, 'INT UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY (id)'
      t.integer :post_id
      t.string :url

      t.timestamps
    end
    
    add_index :post_assets, :post_id, :name => 'post_id'
  end
end
