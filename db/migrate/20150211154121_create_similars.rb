class CreateSimilars < ActiveRecord::Migration
  def change
    create_table :similars, :id => false do |t|
      t.column :id, 'INT UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY (id)'
      t.column :f_id, 'integer unsigned', null: false
      t.column :s_id, 'integer unsigned', null: false
    end

    add_index :similars, [:f_id, :s_id], :unique => true, :name => 'fs_id'
  end
end