class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts, :id => false do |t|
      t.column :id, 'INT UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY (id)'
      t.column :category_id, 'integer unsigned', null: false
      t.column :channel_id, 'integer unsigned', null: false
      t.column :channel_category_id, 'integer unsigned', null: false
      t.string :title, null: false
      t.text :description
      t.text :body
      t.string :source
      t.column :views, 'integer unsigned', null: false, default: 0
      t.column :rating, 'integer unsigned', null: false, default: 0
      t.boolean :is_photo, null: false, default: 0
      t.boolean :is_video, null: false, default: 0
      t.boolean :is_main, null: false, default: 0
      t.boolean :view_type, null: false, default: 0
      t.boolean :status, null: false, default: 0
      t.attachment :main_image
      t.string :main_image_url

      t.column :self_share_count, 'integer unsigned', null: false, default: 0
      t.column :self_like_count, 'integer unsigned', null: false, default: 0
      t.column :self_comment_count, 'integer unsigned', null: false, default: 0
      t.column :self_total_count, 'integer unsigned', null: false, default: 0

      t.column :share_count, 'integer unsigned', null: false, default: 0
      t.column :like_count, 'integer unsigned', null: false, default: 0
      t.column :comment_count, 'integer unsigned', null: false, default: 0
      t.column :total_count, 'integer unsigned', null: false, default: 0

      t.column :total_total, 'integer unsigned', null: false, default: 0

      t.timestamps null: false, default: '0000-00-00 00:00:00'
    end
    
    add_index :posts, :category_id, :name => 'category_id'
    add_index :posts, :channel_id, :name => 'channel_id'
    add_index :posts, :channel_category_id, :name => 'channel_category_id'
    add_index :posts, :source, :unique => true, :name => 'source'
    add_index :posts, :views, :name => 'views'
    add_index :posts, :rating, :name => 'rates'
    add_index :posts, :is_photo, :name => 'is_photo'
    add_index :posts, :is_video, :name => 'is_video'
    add_index :posts, :view_type, :name => 'view_type'
    add_index :posts, :status, :name => 'status'
  end
end
