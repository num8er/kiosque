class CreateSeo < ActiveRecord::Migration
  def change
    create_table :seos, :id => false do |t|
      t.column :id, 'INT UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY (id)'
      t.string :title
      t.string :slug, null: false
      t.string :slug_hash, limit: 32, null: false
      t.column :seoable_id, 'integer unsigned', limit: 10, null: false
      t.string :seoable_type, limit: 16, null: false
    end
    
    add_index :seos, :slug, :unique => true, :name => 'slug'
    add_index :seos, :slug_hash, :unique => true, :name => 'slug_hash'
    add_index :seos, [:seoable_id, :seoable_type], :name => 'seoable'
  end
end
