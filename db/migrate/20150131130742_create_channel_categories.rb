class CreateChannelCategories < ActiveRecord::Migration
  def change
    create_table :channel_categories, :id => false do |t|
      t.column :id, 'INT UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY (id)'
      t.string :name
      t.string :description
      
      t.timestamps null: false, default: '0000-00-00 00:00:00'
    end
  end
end
