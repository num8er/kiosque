class CreateQueryStats < ActiveRecord::Migration
  def change
    create_table :query_stats, :id => false do |t|
      t.column :id, 'INT UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY (id)'
      t.column :query_id, 'integer unsigned', null: false
      t.column :ip, 'integer unsigned', null: false

      t.timestamps
    end
    add_index :query_stats, :query_id, :name => 'query_id'
    add_index :query_stats, :ip, :name => 'ip'
  end
end
