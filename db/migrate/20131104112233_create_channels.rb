class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels, :id => false do |t|
      t.column :id, 'INT UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY (id)'
      t.column :category_id, 'integer unsigned', null: false
      t.column :channel_category_id, 'integer unsigned', null: false
      t.string :title, null: false
      t.string :channel_url, null: false
      t.string :title_path
      t.string :descr_path
      t.string :image_path
      t.string :content_path
      t.string :news_item_path
      t.string :hyperlink_item_path
      t.text :not_allowed_path
      t.string :channel_type, null: false, default: "html"
      t.boolean :status, null: false, default: 1

      t.timestamps null: false, default: '0000-00-00 00:00:00'
    end
    
    add_index :channels, :status, :name => 'status'
    add_index :channels, :channel_type, :name => 'channel_type'
    add_index :channels, :channel_category_id, :name => 'channel_category_id'
  end
end
