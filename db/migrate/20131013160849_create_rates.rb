class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates, :id => false do |t|
      t.column :id, 'INT UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY (id)'
      t.column :post_id, 'integer unsigned', null: false
      t.column :ip, 'integer unsigned', null: false

      t.timestamps null: false, default: '0000-00-00 00:00:00'
    end
    
    add_index :rates, :post_id, :name => 'post_id'
    add_index :rates, :ip, :name => 'ip'
  end
end
