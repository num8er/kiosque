xml.instruct! :xml, :version => "1.0" 
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "Kiosque.az - Самый близкий новостной киоск"
    xml.description "Самый близкий новостной киоск"
    xml.link get_site_url

    for post in @posts
      xml.item do
        xml.title post.title
        xml.description post.description
        xml.pubDate post.created_at.to_s(:rfc822)
        xml.link get_site_url + post.slug
        xml.guid get_site_url + post.slug
      end
    end
  end
end