module ApplicationHelper
  def get_category_name post
    @category ? @category.name : post.category_name
  end
  
  def get_category_seo post
    @category ? @category.seo.slug : post.category.seo.slug
  end
  
  def get_site_name
    @site_name
  end
  
  def get_site_url
    request.protocol + request.host_with_port + "/"
  end
  
  def current_url
    request.url
  end
  
  def current_url_2
    request.url.sub!(request.protocol + request.host_with_port, '')
  end
  
  def is_active source
    if source.kind_of?(Array)
      source.each do |slug|
        return 'active' if request.url.include?(slug)
      end
    else
      request.url.include?(source) ? 'active' : ''
    end
  end
  
  def can_like post
    rate = post.rates.where(['rates.ip = ?', @ip]).order('rates.created_at DESC').first()
    true if rate && ((Time.zone.now - rate.created_at) / 1.day).to_i <= 0
  end
end