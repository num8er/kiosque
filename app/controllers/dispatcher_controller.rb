# encoding: UTF-8
class DispatcherController < ApplicationController

  def categories_json
    categories = Category.all
    response_json = Array.new

    categories.each do |item|
      response_json.push({
        id: item.id,
        title: item.name
      })
    end

    respond_to do |format|
      format.json { render :json => response_json }
    end

  end

  def news_json
    news = Array.new
    if params.has_key?(:id)
      ids = params[:id].map(&:to_i).join(",")
      news = Post.where("category_id IN(" + ids + ")").paginate(:page => params[:page])
    elsif params.has_key?(:pid)
      ids = params[:pid].map(&:to_i).join(",")
      news = Post.where("id IN(" + ids + ")").paginate(:page => params[:page])
    end

    response_json = Hash.new
    response_json["total"] = news.total_pages
    response_json["news"]  = []

    news.each do |item|
      response_json["news"].push({
        id: item.id,
        title: item.title,
        date: item.created_at,
        image: 'http://now.az' + item.main_image.url(:thumb_one),
        text: item.body
      })
    end

    respond_to do |format|
      format.json { render :json => response_json }
    end

  end
  
  def detect
    @action = "index"
    if params.has_key?(:slug)
      if params[:slug] == 'search'
        @action = 'search'
      elsif params[:slug] == 'photo'
        @action = 'photo'
      elsif params[:slug] == 'video'
        @action = 'video'
      elsif params[:slug] == 'c'
        @action = 'category'
      elsif params[:slug] == 'n'
        @action = 'post'
      end
    end
    run
  end
  
  def like
    id   = params[:id]
    ip   = ActiveRecord::Base.connection.execute("SELECT INET_ATON('" + request.remote_ip + "')").first.first
    days = 1
    post = Post.find(id)
    
    last_rate = post.rates.order("created_at DESC").where(:ip => ip).first()

    if last_rate
      days = ((Time.zone.now - last_rate.created_at) / 1.day).to_i
    else
      days = 1
    end
    
    if days > 0
      post.rates.build(:ip => ip)
      post.rating += 1
      post.save
    end
    
    render :json => { rating: post.rating }
  end
  
  def rss
    @posts = Post.where(:status => 1).order("created_at DESC").limit(50)
    respond_to do |format|
      format.html { render :layout => false, :template => "dispatcher/feed" }
      format.rss { render :layout => false, :template => "dispatcher/feed" }
    end
  end

  def about
    render :template => 'page/about'
  end

  def contacts
    render :template => 'page/contacts'
  end
  
  private
  
    def run
      @site_name = "Newsify"
      @current_url = current_url
      #@ip = ActiveRecord::Base.connection.execute("SELECT INET_ATON('" + request.remote_ip + "')").first.first
      method(@action).call
      render_view
    end
    
    def search
      @posts = Post.none
      if params.has_key?(:q)
        q = params[:q]
        #q = Sanitize.clean(q)
        q = q.encode!("UTF-8")
        @posts = Post.published.where("title LIKE :q", q: "%#{q}%").paginate(:page => params[:page])
      end
      save_query q
    end

    def index
      #@main_post = Post.main_post.published.first
      #@posts = Post.published.where.not(id: @main_post.id).order(created_at: :desc).paginate(:page => params[:page])
      #@populars = Post.published.order(views: :desc).take(9)
    end

    def photo
      @posts = Post.published.where(is_photo: true).order(created_at: :desc).paginate(:page => params[:page])
      @populars = Post.published.where(is_photo: true).order(total_total: :desc).take(20)
    end

    def video
      @posts = Post.published.where(is_video: true).order(created_at: :desc).paginate(:page => params[:page])
      @populars = Post.published.where(is_video: true).order(total_total: :desc).take(20)
    end

    def post
      @action = 'post'
      @post = Post.published.find(params[:id])
      @post.views += 1
      @post.save
      
      @populars = Post.published.where.not(id: @post.id).order(total_total: :desc).take(5)
      @similars = @post.similars.order('total_total DESC').limit(5).includes(:post).where("posts.status = ?", "1")
      @first_similar_post = @similars.first
    end
    
    def category
      @category = Seo.find_by(slug: params[:id]).seoable
      @posts = @category.posts.published.order(created_at: :desc).paginate(:page => params[:page])
      @populars = Post.published.where(category_id: @category.id).order(total_total: :desc).take(20)
    end
    
    def render_view
      if request.xhr?
        render :template => @action + '/show', :layout => 'without'
      else
        render :template => @action + '/show'
      end
    end
    
    def save_query query
      query_hash = Digest::MD5.hexdigest(query)
      
      q = Query.find_by(query_hash: query_hash)
    
      unless q
        q = Query.new
        q.query = query
        q.query_hash = query_hash
        q.save
      end
      
      ip = ActiveRecord::Base.connection.execute("SELECT INET_ATON('" + request.remote_ip + "')").first.first
      days = 0
    
      stat = q.query_stats.order("created_at DESC").where(:ip => ip).first()

      if stat
        days = ((Time.zone.now - stat.created_at) / 1.day).to_i
      else
        days = 1
      end
    
      if days > 0
        q.query_stats.build(:ip => ip)
        q.query = query
        q.save
        q.touch
      end
      
    end
end