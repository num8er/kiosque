class ApplicationController < ActionController::Base
  
  include ApplicationHelper
  
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  skip_before_filter :verify_authenticity_token
  before_filter :switch_db_connection
  
  def switch_db_connection
    host = request.host
    connection = "development"
    if host.include?("az")
      connection = "newsifyaz"
    elsif host.include?("ru")
      connection = "newsifyru"
    end
    
    ActiveRecord::Base.configurations = YAML::load(ERB.new(IO.read(File.join(Rails.root, 'config', 'database.yml'))).result)
    ActiveRecord::Base.establish_connection("#{connection}") 
  end
end
