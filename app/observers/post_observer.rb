class PostObserver < ActiveRecord::Observer
  def after_create(post)
    seo = Seo.new
    seo.title = post.title
    seo.seoable_id = post.id
    seo.seoable_type = "Post"
    seo.save
  end
end