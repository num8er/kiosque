class CategoryObserver < ActiveRecord::Observer
  def before_save(category)
    category.sort = Category.maximum("sort") + 1 if category.sort == 0
  end
  
  def after_create(category)
    seo = Seo.new
    seo.title = category.name
    seo.seoable_id = category.id
    seo.seoable_type = "Category"
    seo.save
  end
end
