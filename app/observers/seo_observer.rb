class SeoObserver < ActiveRecord::Observer
  def before_save(seo)
    seo.slug_hash = Digest::MD5.hexdigest(seo.slug)
  end
end
