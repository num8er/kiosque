ThinkingSphinx::Index.define :post, :with => :active_record do
  indexes title, body

  where sanitize_sql(["status", true])
  
  has id
end