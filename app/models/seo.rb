class Seo < ActiveRecord::Base
  belongs_to :seoable, polymorphic: true
  acts_as_url :title, { :url_attribute => "slug" }
end