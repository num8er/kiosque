class Post < ActiveRecord::Base
  has_one :seo, as: :seoable, dependent: :destroy
  has_many :post_assets, dependent: :destroy
  has_many :rates, dependent: :destroy
  has_many :similars, :foreign_key => :f_id, dependent: :destroy
  belongs_to :category
  belongs_to :channel
  belongs_to :channel_category
  
  accepts_nested_attributes_for :seo, :allow_destroy => true
  accepts_nested_attributes_for :post_assets, :allow_destroy => true
  
  self.per_page = 18
  
  has_attached_file :main_image, 
    :url => "/uploads/:class/:id/:style/:basename.:extension",
    :path => ":rails_root/public/uploads/:class/:id/:style/:basename.:extension",
    :styles => {
      :thumb   => "700x700>",
    },
    :convert_options => { :thumb => '-quality 95' },
    :default_url => "/images/:style/missing.png"
  
  validates_attachment :main_image, 
    :presence => true,
    :content_type => { :content_type => /^image\/(png|gif|jpg|jpeg)/ },
    :size => { :in => 0..2000.kilobytes }
  
  #default_scope :order => 'posts.created_at DESC'
  
  scope :published, -> { where(:status => 1) }
  scope :unpublished, -> { where(:status => 0) }
  scope :main_post, -> { where(:is_main => 1) }
  scope :only_photo, -> { published.where(:is_photo => 1) }
  scope :only_video, -> { published.where(:is_video => 1) }
  scope :only_text, -> { published.where(:is_video => 0).where(:is_photo => 0) }
  
  def posted_at
    self.created_at.strftime("%d.%m.%Y %H:%M")
  end

  def category_slug
    self.category.slug
  end
  
  def category_name
    self.category.name
  end
  
  def link
    '/n/' + self.id.to_s
  end
  
  def type
    type = "text"
    type = "image" if self.is_photo
    type = "video" if self.is_video
    type = "image video" if self.is_photo && self.is_video
    type
  end
  
  def type_icons
    icons = []
    icons << "image" if self.is_photo
    icons << "video" if self.is_video
    icons << "text" unless self.is_photo || self.is_video
    
    type_icons = ''
    
    icons.each do |icon|
      type_icons += '<i class="' + icon + '" title="' + icon + ' news"></i>'
    end
    
    type_icons
  end
  
  def main_image_by_url=(url)
    io = open(URI.parse(url))
    self.main_image = io
    self.main_image_file_name = io.base_uri.path.split('/').last
  end
end
