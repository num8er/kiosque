class Category < ActiveRecord::Base
  has_one :seo, as: :seoable, dependent: :destroy
  has_many :posts
  
  default_scope :order => 'sort ASC'
  
  def slug
    '/c/' + self.seo.slug
  end
end
