class Query < ActiveRecord::Base
  has_many :query_stats, dependent: :destroy
  
  scope :qs_today, lambda{ |selected_date| where('query_stats.updated_at' => selected_date.beginning_of_day..selected_date.end_of_day) }
  scope :today, lambda{ |selected_date| qs_today(selected_date).where(:updated_at => selected_date.beginning_of_day..selected_date.end_of_day) } 
end
