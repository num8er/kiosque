class Ckeditor::Picture < Ckeditor::Asset
  has_attached_file :data,
                    :url  => "/uploads/images/:id/:style/:basename.:extension",
                    :path => ":rails_root/public/uploads/images/:id/:style/:basename.:extension",
                    :styles => {
                      :small => "290x142>",
                      :medium => "290x193>",
                      :content => '800>'
                    }

  validates_attachment_size :data, :less_than => 1.megabytes
  validates_attachment_presence :data

  def url_content
    url(:content)
  end
end
