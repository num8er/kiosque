class ChannelCategory < ActiveRecord::Base
  has_many :posts, dependent: :destroy
  
  scope :published, -> { where(:status => 1)}
  scope :unpublished, -> { where(:status => 0)}
end