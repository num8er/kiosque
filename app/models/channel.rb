class Channel < ActiveRecord::Base
  has_many :posts, dependent: :destroy
  belongs_to :category
  belongs_to :channel_category
  
  scope :published, -> { where(:status => 1)}
  scope :unpublished, -> { where(:status => 0)}
end