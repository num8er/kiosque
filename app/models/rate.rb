class Rate < ActiveRecord::Base
  belongs_to :posts
  
  default_scope :order => 'created_at DESC'
end
