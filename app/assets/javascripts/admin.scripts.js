$(document).ready(function(){

	$(document).on('click', '.main_image_select', function(){
		var t = $(this),
			mainImage = t.parent().find('.main_image'),
			postId = mainImage.attr('dataid'),
			imageUrl = t.attr('dataurl'),
			request = $.ajax({
			  url: '/admin/posts/'+postId+'/set_main_image',
			  type: "GET",
			  data: { url : imageUrl },
			  dataType: "json"
			});
			
		request.done(function( data ) {
			if (data[0] == 'success') mainImage.html('<img src="'+imageUrl+'">');
		});
	});

	$(document).on('change', '.category_select', function(){
		var t = $(this),
			postId = t.attr('dataid'),
			categoryId = t.val(),
			request = $.ajax({
			  url: '/admin/posts/'+postId+'/set_category',
			  type: "GET",
			  data: { cid : categoryId },
			  dataType: "json"
			});
			
		request.done(function( data ) {
			//if (data[0] == 'success') alert('done!');
		});
	});

	$(document).on('change', '.is_photo', function(){
		var t = $(this),
			postId = t.attr('dataid'),
			tVal = t.val(),
			request = $.ajax({
			  url: '/admin/posts/'+postId+'/set_is_photo',
			  type: "GET",
			  data: { value : tVal },
			  dataType: "json"
			});
			
		request.done(function( data ) {
			//if (data[0] == 'success') alert('done!');
		});
	});
	
	$(document).on('change', '.is_video', function(){
		var t = $(this),
			postId = t.attr('dataid'),
			tVal = t.val(),
			request = $.ajax({
			  url: '/admin/posts/'+postId+'/set_is_video',
			  type: "GET",
			  data: { value : tVal },
			  dataType: "json"
			});
			
		request.done(function( data ) {
			//if (data[0] == 'success') alert('done!');
		});
	});
	
	$(document).on('change', '.is_main', function(){
		var t = $(this),
			postId = t.attr('dataid'),
			tVal = t.val(),
			request = $.ajax({
			  url: '/admin/posts/'+postId+'/set_is_main',
			  type: "GET",
			  data: { value : tVal },
			  dataType: "json"
			});
			
		request.done(function( data ) {
			//if (data[0] == 'success') alert('done!');
		});
	});
	
	$(document).on('change', '.view_type', function(){
		var t = $(this),
			postId = t.attr('dataid'),
			tVal = t.val(),
			request = $.ajax({
			  url: '/admin/posts/'+postId+'/set_is_iframe',
			  type: "GET",
			  data: { value : tVal },
			  dataType: "json"
			});
			
		request.done(function( data ) {
			//if (data[0] == 'success') alert('done!');
		});
	});
	
	$(document).on('click', '.publish_status', function(){
		var t = $(this),
			postId = t.attr('dataid'),
			request = $.ajax({
			  url: '/admin/posts/'+postId+'/set_published',
			  type: "GET",
			  data: { },
			  dataType: "json"
			});
			
		request.done(function( data ) {
			if (data[0] == 'success') $('tr#post_'+postId).remove();
		});
		return false;
	});

});