ActiveAdmin.register Channel do
  
  menu :priority => 3
  
  config.paginate = false
  config.filters = false
  
  scope :all, :default => true
  scope :published
  scope :unpublished
  
  index do
    column :title
    column :category
    column :channel_type
    bool_column :status
    column() {|channel| link_to('Parse', parse_admin_channel_path(channel)) }
    default_actions
  end
  
  member_action :parse do
    #system("php ~/Desktop/newsifyru/parser/parseSingle.php " + params[:id] + " &")

    host = request.host
    zone = "ru"
    if host.include?("az")
      zone = "az"
    elsif host.include?("ru")
      zone = "ru"
    end

    system("php /home/newsify/parser/parseSingle.php " + params[:id] + " " + zone + " &")
    redirect_to :action => :index
  end
  
  action_item :only => [:show] do
    link_to('Parse', parse_admin_channel_path(channel))
  end
  
  controller do
    def permitted_params
      params.permit(:channel => [
        :category_id,
        :channel_category_id,
        :title,
        :channel_url,
        :title_path,
        :descr_path,
        :image_path,
        :content_path,
        :news_item_path,
        :hyperlink_item_path,
        :not_allowed_path,
        :channel_type,
        :status
      ])
    end
  end
end