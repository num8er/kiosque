ActiveAdmin.register PostAsset do

  menu false
  
  controller do
    def permitted_params
      params.permit(:post_asset => [:post_id, :url])
    end
  end
end
