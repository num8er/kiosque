ActiveAdmin.register Post do
  
  menu :priority => 4
  
  config.sort_order = "created_at_desc"
  config.per_page = 50
  #config.clear_action_items!
  
  STATUSES = { :yes => 1, :no => 0 }
  
  scope :all
  scope :published
  scope :unpublished, :default => true
  
  filter :channel_category
  filter :title
  filter :category
  filter :is_photo, :as => :select, :collection => STATUSES
  filter :is_video, :as => :select, :collection => STATUSES
  filter :is_main, :as => :select, :collection => STATUSES
  filter :created_at
  
  index do
    selectable_column
    column :channel
    column :source do |post|
      a :href => post.source, :target => "_blank" do
        post.source
      end
    end
    column :title
    column :settings do |post|
      div :for => post do
        div do
          label do
            h6 do "Is Photo" end
            select :class => 'is_photo', :dataid => post.id do
              option :value => 0, :selected => false == post.is_photo ? "selected" : "" do
                "no"
              end
              option :value => 1, :selected => true == post.is_photo ? "selected" : "" do
                "yes"
              end
            end
          end
          label do
            h6 do "Is Video" end
            select :class => 'is_video', :dataid => post.id do
              option :value => 0, :selected => false == post.is_video ? "selected" : "" do
                "no"
              end
              option :value => 1, :selected => true == post.is_video ? "selected" : "" do
                "yes"
              end
            end
          end
          label do
            h6 do "Is Main" end
            select :class => 'is_main', :dataid => post.id do
              option :value => 0, :selected => false == post.is_main ? "selected" : "" do
                "no"
              end
              option :value => 1, :selected => true == post.is_main ? "selected" : "" do
                "yes"
              end
            end
          end
          label do
            h6 do "Is Iframe" end
            select :class => 'view_type', :dataid => post.id do
              option :value => 0, :selected => false == post.view_type ? "selected" : "" do
                "no"
              end
              option :value => 1, :selected => true == post.view_type ? "selected" : "" do
                "yes"
              end
            end
          end
        end
        div do
          select :class => 'category_select', :dataid => post.id do
            option :value => 0 do
              "select category"
            end
            Category.all.collect do |c|
              option :value => c.id, :selected => c.id == post.category_id ? "selected" : "" do
                c.name
              end
            end
          end
        end
        div :class => 'main_image', :dataid => post.id do
          img :src => post.main_image.url if post.main_image?
        end
        if post.post_assets.count > 0
          post.post_assets.collect do |asset|
            div image_tag(asset.url, :width => 75), :class => 'main_image_select', :dataurl => asset.url
          end
        end
      end
      div :class => 'publish_status_div' do
        button :class => 'publish_status', :dataid => post.id do
          "publish"
        end
      end
    end
    column :created_at
    column :updated_at
    default_actions
  end
  
  member_action :set_main_image do
    p = Post.find(params[:id])
    
    response = ["error"]
    if p
      p.main_image_by_url = params[:url]
      p.save
      response = ["success"]
    end

    respond_to do |format|
      format.json { render json: response}
    end
  end
  
  member_action :set_category do
    p = Post.find(params[:id])
    
    response = ["error"]
    if p
      p.category_id = params[:cid]
      p.save
      response = ["success"]
    end

    respond_to do |format|
      format.json { render json: response}
    end
  end
  
  member_action :set_is_photo do
    p = Post.find(params[:id])
    
    response = ["error"]
    if p
      p.is_photo = params[:value]
      p.save
      response = ["success"]
    end

    respond_to do |format|
      format.json { render json: response}
    end
  end
  
  member_action :set_is_video do
    p = Post.find(params[:id])
    
    response = ["error"]
    if p
      p.is_video = params[:value]
      p.save
      response = ["success"]
    end

    respond_to do |format|
      format.json { render json: response}
    end
  end
  
  member_action :set_is_main do
    p = Post.find(params[:id])
    
    response = ["error"]
    if p
      p.is_main = params[:value]
      p.save
      response = ["success"]
    end

    respond_to do |format|
      format.json { render json: response}
    end
  end
  
  member_action :set_is_iframe do
    p = Post.find(params[:id])
    
    response = ["error"]
    if p
      p.view_type = params[:value]
      p.save
      response = ["success"]
    end

    respond_to do |format|
      format.json { render json: response}
    end
  end
  
  member_action :set_published do
    p = Post.find(params[:id])
    
    response = ["error"]
    if p
      p.status = 1
      p.save
      response = ["success"]
    end

    respond_to do |format|
      format.json { render json: response}
    end
  end
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Details" do
      f.input :channel
      f.input :channel_category
      f.input :category
      #f.has_many :seo do |cf|
      #  cf.input :slug
      #end
      f.input :title
      f.input :description
      f.input :source
      f.input :body, :as => :ckeditor
      f.input :is_photo
      f.input :is_video
      f.input :is_main
      f.input :view_type, :label => 'Is Iframe'
      f.input :status, :label => 'Is Active'
      f.input :main_image, :as => :radio, :label => "Choose image", :collection => if post.post_assets.count > 0 
        post.post_assets.collect {|asset| [image_tag(asset.url, :width => 200), asset.url] }
      else
        [[image_tag(post.main_image, :width => 200), post.main_image]]
      end

      f.input :main_image, :label => "or upload new image", :hint => f.template.image_tag(post.main_image, :width => "300")
      #f.has_many :post_assets, :allow_destroy => true, :heading => 'Images' do |cf|
      #  cf.input :url, :hint => cf.template.image_tag(cf.object.url, :width => "300")
      #end
    end
    f.actions
  end
  
  controller do
    def scoped_collection
      Post.unscoped
    end
        
    def permitted_params
      params.permit(:post => [:category_id, :channel_id, :title, :description, :source, :body, :main_image, :is_photo, :is_video, :view_type, :status, post_assets_attributes: [:id, :url, :_destroy], seo_attributes: [:id, :slug]])
    end
  end
end
