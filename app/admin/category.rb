ActiveAdmin.register Category do
  
  menu :priority => 2
  
  config.sort_order = "sort_asc"
  config.paginate = false
  config.filters = false
  
  index do
    column :sort
    column :name
    column :seo do |seo|
      seo.slug
    end
    default_actions
  end
  
  controller do
    def scoped_collection
      Category.unscoped
    end
    
    def permitted_params
      params.permit(:category => [:name, :sort])
    end
  end
end
