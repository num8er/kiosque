ActiveAdmin.register Query, :as => "Statistic" do
  
  menu :priority => 5
  
  config.sort_order = "count_desc"
  config.filters = false
  config.clear_action_items!

  index do
    selectable_column
    column :query, :sortable => false
    column :count
    column :updated_at, :sortable => false
  end
  
  action_item only: :index do
    if params[:day]
      order = "count_desc"
      
      if params[:order]
        order = params[:order]
      end
      
      day = Time.zone.now
      pday = params[:day].to_date
      if pday != day.to_date
        link_to('Today', '/admin/statistics?day=' + day.to_date.to_s + '&order=' + order)
      end
    end
  end
  
  action_item only: :index do
    order = "count_desc"

    if params[:order]
      order = params[:order]
    end
    
    if params[:day]
      yesterday = (params[:day].to_date - 1.day).to_s
    else
      yesterday = (Time.zone.now.to_date - 1.day).to_s
    end
    link_to('< ' + yesterday, '/admin/statistics?day=' + yesterday + '&order=' + order)
  end
  
  action_item only: :index do
    order = "count_desc"

    if params[:order]
      order = params[:order]
    end
    
    if params[:day]
      day = Time.zone.now
      pday = params[:day].to_date
      if pday != day.to_date
        tomorrow = (pday + 1.day).to_s
        link_to(tomorrow + ' >', '/admin/statistics?day=' + tomorrow + '&order=' + order)
      end
    end
  end
  
  controller do
    def scoped_collection
      day = Time.zone.now
      day = params[:day].to_date if params[:day] 
      Query.select("queries.*, COUNT(query_stats.id) AS count").joins(:query_stats).today(day).group("queries.id")
    end
  end
end
