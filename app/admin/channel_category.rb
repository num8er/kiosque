ActiveAdmin.register ChannelCategory do
  
  menu :priority => 4
  
  config.paginate = false
  config.filters = false
  
  index do
    column :name
    default_actions
  end
  
  controller do
    def permitted_params
      params.permit(:channel_category => [
        :name,
        :description
      ])
    end
  end
end